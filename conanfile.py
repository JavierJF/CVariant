from conans import ConanFile, CMake, tools
import os

class CVariantConan(ConanFile):
    name = "CVariant"
    version = "0.1"
    license = "Raising The Floor"
    url = "https://gitlab.com/MiMiC-Framework/CVariant"
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "tests": [True, False]}
    generators = "cmake"
    requires = "gtest/1.8.0@bincrafters/stable"
    default_options = (
        "shared=False",
        "tests=False"
    )

    def source(self):
        self.run("git clone https://gitlab.com/MiMiC-Framework/CVariant")
        self.run("cd CVariant && git checkout develop")

    def build(self):
        cmake = CMake(self, parallel=True)
        if self.options.tests:
            cmake.definitions["CMAKE_BUILD_TESTS"] = "ON"
        cmake.configure(source_folder="CVariant")
        cmake.build(target="install")

    def package(self):
        self.copy("*.h", dst="include", src="CVariant/include")
        self.copy("*CVariant.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["CVariant"]
