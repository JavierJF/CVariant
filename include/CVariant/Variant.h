#ifndef VARIANT_H
#define VARIANT_H

#include <stdint.h>

extern "C" {

struct NArray;
struct CArray;
struct GArray;
struct NGArray;

enum VariantType {
    VT_Bool         = 0,
    VT_Char         = 1,
    VT_Int8         = 2,
    VT_Int16        = 3,
    VT_Int32        = 4,
    VT_UInt8        = 5,
    VT_UInt16       = 6,
    VT_UInt32       = 7,
    VT_LongInt      = 8,
    VT_Float        = 9,
    VT_Double       = 11,
    VT_LongDouble   = 12,
    VT_PChar        = 13,
    VT_PVoid        = 14,
    VT_CArray       = 0x2000,
    VT_NArray       = 0x4000,
    VT_GArray       = 0x6000,
    VT_NGArray      = 0x8000
};

struct Variant {
    enum VariantType type;
    union {
        bool            bVal;
        char            cVal;
        int8_t          i8Val;
        int16_t         i16Val;
        int32_t         i32Val;
        uint8_t         ui8Val;
        uint16_t        ui16Val;
        uint32_t        ui32Val;
        long int        lIVal;
        float           fVal;
        double          dVal;
        long double     ldVal;
        char*           pChar;
        void*           pVoid;
        struct NArray*  nArray;
        struct CArray*  cArray;
        struct GArray*  gArray;
        struct NGArray* ngArray;
    };
};

}

/**
 * @brief
 *  Equalilty comparison for Variant types. This comparison only works for the types of Variants
 *  which doesn't hide void pointers, those ones needs to be defined by the final user of the class.
 * @param var1
 *  First variant for equaility comparison.
 * @param var2
 *  Second variant for equaility comparison.
 * @return
 *  True if boths are equals, false if not.
 */
bool operator==(const Variant& var1, const Variant& var2);

#endif
