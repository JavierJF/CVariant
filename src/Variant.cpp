#include <CVariant/Variant.h>

bool operator==(const Variant& var1, const Variant& var2) {
    bool sameType = var1.type == var2.type;

    if (!sameType) {
        return false;
    }

    auto type = var1.type;

    if (type == VT_Bool) {
        return var1.bVal == var2.bVal;
    } else if (type == VT_Char) {
        return var1.cVal == var2.cVal;
    } else if (type == VT_Int8) {
        return var1.i8Val == var2.i8Val;
    } else if (type == VT_Int16) {
        return var1.i16Val == var2.i16Val;
    } else if (type == VT_Int32) {
        return var1.i32Val == var2.i32Val;
    } else if (type == VT_UInt8) {
        return var1.ui8Val == var2.ui8Val;
    } else if (type == VT_UInt16) {
        return var1.ui16Val == var2.ui16Val;
    } else if (type == VT_UInt32) {
        return var1.ui32Val == var2.ui32Val;
    } else if (type == VT_LongInt) {
        return var1.lIVal == var2.lIVal;
    } else if (type == VT_Float) {
        return var1.fVal == var2.fVal;
    } else if (type == VT_Double) {
        return var1.dVal == var2.dVal;
    } else if (type == VT_LongDouble) {
        return var1.ldVal == var2.ldVal;
    } else if (type == VT_PChar) {
        return (*var1.pChar) == (*var2.pChar);
    } else if (type == VT_PVoid) {
        return false;
    } else if (type == VT_CArray) {
        return false;
    } else if (type == VT_NArray) {
        return false;
    } else if (type == VT_GArray) {
        return false;
    } else if (type == VT_NGArray) {
        return false;
    } else {
        return false;
    }
}
