#include <CVariant/Variant.h>
#include <gtest/gtest.h>

TEST( useVariant, Variant )
{
    struct Variant vFloat { VT_Float };
    vFloat.fVal = 1.0;

    ASSERT_TRUE(vFloat.fVal);
}